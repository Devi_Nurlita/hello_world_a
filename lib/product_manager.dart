import 'package:flutter/material.dart';
import 'package:hello_world_a/product_control.dart';
import 'products.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;
  ProductManager({this.startingProduct = 'Sweet Tester'});

  @override
  _ProductManagerState createState() {
    print('[_ProductManagerState] createState()');
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = ['Food Tester'];

  @override
  void initState(){
    print('[ProductManagerState] initstate()');
    _products.add(widget.startingProduct);
    super.initState();
  }

  @override
  void didUpdateWidget(ProductManager oldWidget){
    print('[ProductManager State] didUpdateWidget');
    super.didUpdateWidget(oldWidget);
  }

  void _addProduct(String product){
    setState(() {
          _products.add(product);
          print(_products);
    });
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerState] initState()');
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
            color: Theme.of(context).primaryColor,
            child: ProductControl(_addProduct),
        ),
        Products(_products),
      ],
    );
  }
}
