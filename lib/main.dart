import 'package:flutter/material.dart';
import 'product_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
      ),
      title: 'Hello World',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Aplikasiku'),
        ),
        body: ProductManager(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
